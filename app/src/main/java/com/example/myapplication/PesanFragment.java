package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PesanFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pesan, container, false);

        RecyclerView rcBerita = v.findViewById(R.id.recyclerview_pesan);
        rcBerita.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcBerita.setAdapter(new AdapterPesan(getActivity()));

        return v;
    }
}