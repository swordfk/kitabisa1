package com.example.myapplication;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class DonasiFragment extends Fragment {
    RecyclerView rckebaikan, rcPilihan, rcDoa, rcKategori;
    SliderView sliderView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_donasi, container, false);

        rckebaikan = v.findViewById(R.id.recyclerview_kebaikan);
        rcPilihan = v.findViewById(R.id.recyclerview_pilihan);
        rcKategori = v.findViewById(R.id.recyclerview_kategori);
        rcDoa = v.findViewById(R.id.recyclerview_doa);
        sliderView = v.findViewById(R.id.imageSlider);

        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rckebaikan.setLayoutManager(horizontalLayoutManager);
        rckebaikan.setAdapter(new AdapterKebaikan(getActivity()));

        //Slider
        List<SliderItem> sliderItemList = new ArrayList<>();
        SliderItem sliderItem = new SliderItem(R.drawable.slider);
        SliderItem sliderItem2 = new SliderItem(R.drawable.slider);
        SliderItem sliderItem3 = new SliderItem(R.drawable.slider);
        sliderItemList.add(sliderItem);
        sliderItemList.add(sliderItem2);
        sliderItemList.add(sliderItem3);
        SliderAdapterExample adapter = new SliderAdapterExample(getActivity(), sliderItemList);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();

        LinearLayoutManager horizontalLayoutManager2
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rcPilihan.setLayoutManager(horizontalLayoutManager2);
        rcPilihan.setAdapter(new AdapterPilihan(getActivity()));

        LinearLayoutManager horizontalLayoutManager3
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rcDoa.setLayoutManager(horizontalLayoutManager3);
        rcDoa.setAdapter(new AdapterDoa(getActivity()));

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcKategori.setLayoutManager(layoutManager);
        rcKategori.setAdapter(new AdapterKategori(getActivity()));

        return v;
    }
}